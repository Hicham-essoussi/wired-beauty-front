module.exports = 
{
  chainWebpack: config => 
      { config.plugin('html').tap(args => 
          { args[0].title = 'Wired'; return args; }); },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
      enableBridge: false
    }
  },

  transpileDependencies: [
    'vuetify'
  ]
}
