import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";
import WhatWeDo from "../views/WhatWeDo.vue";
import NewNomadic from "../views/NewNomadic.vue";
import SciValidation from "../views/SciValidation.vue";
import WhoWeAre from "../views/WhoWeAre.vue";
import Service from "../views/Service.vue";
import Signup from "../views/Login.vue";
import Login from "../views/Login.vue";
import i18n from "../i18n";
import Languages from "../components/LanguageSwitcher";
import HomeAdmin from "../views/HomeAdmin";
import Admin from "../views/Admin";
import ListPost from "../views/ListPost";
import Customer from "../views/Customer";
import Product from "../views/Product";
import Compaign from "../views/Compaign";
import Question from "../views/Question";
import Tester from "../views/Tester";
import Home from "../views/Home";
import TesterProfile from "../views/TesterProfile";
import TesterCompaign from "../views/TesterCompaign";
import LoginTester from "../views/LoginTester.vue";
import TesterAdmin from "../views/TesterAdmin";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: `/${i18n.locale}`,
  },
  {
    path: "/:lang",
    component: {
      render(c) {
        return c("router-view");
      },
    },
    children: [
      {
        path: "/",
        name: "Home",
        component: Home,
      },

      {
        path: "Admin",
        name: "Admin",
        component: Admin,
      },
      {
        path: "Tester",
        name: "Tester",
        component: Tester,
      },
      {
        path: "Customer",
        name: "Customer",
        component: Customer,
      },
      {
        path: "Product",
        name: "Product",
        component: Product,
      },

      {
        path: "ListPost",
        name: "ListPost",
        component: ListPost,
      },

      {
        path: "Compaign",
        name: "Compaign",
        component: Compaign,
      },
      {
        path: "Question",
        name: "Question",
        component: Question,
      },

      {
        path: "WhatWeDo",
        name: "WhatWeDo",
        component: WhatWeDo,
      },
      {
        path: "NewNomadic",
        name: "NewNomadic",
        component: NewNomadic,
      },
      {
        path: "SciValidation",
        name: "SciValidation",
        component: SciValidation,
      },
      {
        path: "WhoWeAre",
        name: "WhoWeAre",
        component: WhoWeAre,
      },
      {
        path: "Service",
        name: "Service",
        component: Service,
      },
      {
        path: "Signup",
        name: "Signup",
        component: Signup,
      },
      {
        path: "Login",
        name: "Login",
        component: Login,
      },
      {
        path: "Languages",
        name: "Languages",
        component: Languages,
      },
      {
        path: "HomeAdmin",
        name: "HomeAdmin",
        component: HomeAdmin,
      },
      {
        path: "TesterCompaign",
        name: "Compaign",
        component: TesterCompaign,
      },
      {
        path: "TesterAdmin",
        name: "Admin",
        component: TesterAdmin,
      },
      {
        path: "TesterProfile",
        name: "TesterProfile",
        component: TesterProfile,
      },
      {
        path: "LoginTester",
        name: "LoginTester",
        component: LoginTester,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
